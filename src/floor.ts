import { Mesh, MeshBuilder, PhysicsImpostor } from 'babylonjs';
import { scene } from './scene';

export function createFloor(): Mesh {
    const size: number = 4;
    const floor = MeshBuilder.CreateGround('floor', {width: size, height: size}, scene);
    floor.physicsImpostor = new PhysicsImpostor(
        floor,
        PhysicsImpostor.BoxImpostor,
        { mass: 0, restitution: 0.9 },
        scene
    );
    
    return floor;
}