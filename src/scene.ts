import * as B from 'babylonjs';
import { ArcRotateCamera, Vector3, Scene, HemisphericLight, Color4 } from 'babylonjs';
import { canvas } from './domItems';

export const engine = new B.Engine(canvas, true);
export const scene = createScene();

function createScene(): Scene {
    const scene = new B.Scene(engine);

    createCamera(scene);
    createLight(scene);
    setBackground(scene);

    return  scene;
}

function createCamera(scene: Scene): void {
    const alpha: number = Math.PI / 4;
    const beta: number = Math.PI / 3;
    const radius: number = 8;
    const target = new Vector3(0, 0, 0);

    new ArcRotateCamera(
        'Camera', 
        alpha,
        beta, 
        radius,
        target,
        scene 
    ).attachControl(canvas, true);


}
function createLight(scene: Scene): void {
    new HemisphericLight("light", new Vector3(1, 1, 0), scene);
}
function setBackground(scene: Scene): void {
    scene.clearColor = new Color4(0, 0, 0, 1);
}