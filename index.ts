import 'regenerator-runtime/runtime';
import Ammo from 'ammojs-typed';
import { scene, engine } from './src/scene';
import { createFloor } from './src/floor';
import { AmmoJSPlugin, Vector3 } from 'babylonjs';
import { createCube } from './src/cube';

async function main(): Promise<void> {
    const ammo = await Ammo();
    const physics: AmmoJSPlugin = new AmmoJSPlugin(true,  ammo);

    scene.enablePhysics(new Vector3(0, -9.81, 0), physics);
    createCube();
    createFloor()
    engine.runRenderLoop(() => scene.render());
}

main();